## Run it locally

Run it:
```
pip install -r requirements.txt

MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.0.1 \
	MQTT_PORT=1883 \
	MQTT_NAME=desjardins
	ROOT_TOPIC=homeassistant \
	LOG_LEVEL=DEBUG \
    CONFIG_FILE=settings.yaml \
	env/bin/mqtt_desjardins
```

## Build Docker image

```
docker build -t mqtt-desjardins .
```

## Run Docker container

```
docker run --rm -it \
    --name bttest \
    -e MQTT_USERNAME=hass \
    -e MQTT_PASSWORD=hass \
    -e MQTT_HOST=192.168.0.1 \
    -e MQTT_PORT=1883 \
    -e ROOT_TOPIC=homeassistant \
    -e LOG_LEVEL=INFO \
    -e CONFIG_FILE=settings.yaml \
    mqtt-desjardins:latest bash
```
