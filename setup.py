import sys

from setuptools import setup, find_packages
from mqtt_desjardins.__version__ import VERSION

if sys.version_info < (3,7):
    sys.exit('Sorry, Python < 3.7 is not supported')

install_requires = list(val.strip() for val in open('requirements.txt'))
tests_require = list(val.strip() for val in open('test_requirements.txt'))

setup(name='mqtt-desjardins',
      version=VERSION,
      description=('Daemon collecting data from Desjardins with '
                   'Home-Assistant through MQTT'),
      author='Thibault Cohen',
      author_email='titilambert@ttb.lt',
      url='http://gitlab.com/titilambert/mqtt_desjardins',
      package_data={'': ['LICENSE.txt']},
      include_package_data=True,
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'mqtt_desjardins = mqtt_desjardins.__main__:main',
          ]
      },
      license='Apache 2.0',
      install_requires=install_requires,
      tests_require=tests_require,
      classifiers=[
        'Programming Language :: Python :: 3.7',
      ]
)
